# Gestão de Projetos Usando Gitlab

Este guia foi escrito enquanto se utilizava a versão do GitLab:
```
GitLab Enterprise Edition 15.6.0-pre 64aebb243cb
```
—

## Projeto
Um *projeto* é um esforço temporário empreendido para criar um produto, serviço ou resultado.

### Características de um projeto
- Tem início e fim;
- É elaborado progressivamente;
- Sofre restrições de recursos;
- Possui um custo;
- Existe um risco para se alcançar o resultado desejado.

## Gestão de projetos
Uma boa gestão de um projeto é a chave para a organização/time alcançar os objetivos dentro dos vínculos existentes.

A plataforma GitLab provê funcionalidades/ferramentas flexíveis para o gerenciamento de projetos usando a abordagem Agile, Kanbam e Scrum por exemplo.

Para isso, é interessante compreender algumas das ferramentas principais da plataforma para a gestão de um projeto.

# Marcos (*Milestones*)

Representam os marcos de entrega do projeto. Na metodologia ágil, eles são associados aos *sprints*: um marco por *sprint*.

No GitLab, os marcos são criados dentro de cada projeto, e podem ser acessados tanto por projeto individual quanto por grupo (TODO colocar a indicação do menu)

# Quadros, *issues* e etiquetas (*Boards*, *issues*, *labels*)

## Quadros

Na seção de quadros é possível visualizar um quadro kanban similar ao Trello. 

Por padrão, ele mostra apenas dois quadros:  Open e Closed (pode mudar de acordo com a versão). Recomenda-se que seja adicionada a coluna “Review”.

- **Open**:
	- Requisitos mais abrangentes ou user stories
- **To Do**
	- Tarefas relacionadas às user stories na coluna open
- **Doing**
	- Tarefas nas quais algum membro da equipe já está trabalhando
	- Quando um desenvolvedor move uma tarefa para a coluna doing, deve atribuir a si próprio, a tarefa através do link “Assign yourself” (Clica-se no cartão, não no nome da tarefa)
- **Review**
	- Tarefas “finalizadas”, deven ser revisadas de alguma forma antes de ser fechada.
- **Closed**
	- Tarefas finalizadas.

Vários quadros podem ser criados e associados a cada marco ou *sprint*.

## *Issues*

Faremos a distinção entre dois tipos de issues: as *user stories* (US) e *tarefas*.

### *User Stories*
Para as *issues* que representam *user stories* recomendamos usar, **na coluna Open** usando o seguinte modelo template:

> # Título
> **Como** {ator} 
**quero** {ação}
**para que** {motivo}
> ## Critérios de aceitação
> - ...

É importante que os critérios de aceitação sejam bem específicos e estejam bem documentados.
### Tarefas

*User stories* não são tarefas, mas as possuem. As tarefas são as etapas mais técnicas e são criadas na coluna **To Do**.

Tarefas devem possuir subtarefas detalhando as etapas necessárias para desenvolvê-las, pois isso auxilia na estimativa de esforço necessário.

## Etiquetas (*Labels*)

As etiquetas são utilizadas para dividir as diferentes “áreas” das tarefas (e.g. *backend*, *frontend*, etc). Essa categorização auxilia na filtragem das tarefas.

# Relacionamento Entre Tarefas

As tarefas podem ser vinculadas através do botão “linked issues” dentro da tarefa.

User stories são bloqueadas por tarefas, portanto as adicionamos como “blocked by”.

User stories são movidas diretamente de Open para Closed, quando todas as tarefas que o bloqueam já estão em Closed.

## *Service Desk*

É possível cadastrar um endereço de e-mail para que e-mails recebidos sejam automaticamente convertidos em *issues*.

# Estimativa e Acompanhamento de Tempo e Desempenho

A primeira *user stories* deve servir como âncora para definir o valor das demais. (A US 2 é mais trabalhosa que a 1?)

Cadastramos o peso das USs clicando no cartão.

Ao longo do tempo, se torna possível estimar quantos pontos o time é capaz de completar por sprint.

Recomenda que nas tarefas, a estimativa seja de tempo e não de ponto, caso exista a necessidade de reportar estimativas de tempo por tarefa para o cliente.

Nos comentários das issues, criamos estimativas através do comando

```
# /estimate <número><medida de tempo>
/estimate 4h
```

## O Papel do Desenvolvedor/Gestor

O desenvolvedor/gestor do projeto deve interagir com o quadro diariamente, analisando as *User Stories* mais prioritárias e escolhendo entre as tarefas que as compõem, a tarefa na qual irá trabalhar no dia.

A pessoa que irá atribuir as tarefas em geral tem um papel mais gerencial na metodologia ágil, mas a tarefa pode ser feita pelas pessoas que estão mais familiarizadas com o projeto e as capacidade de cada integrante do time.

### Tarefas não Finalizadas no Final do Dia

Todo final de dia, caso a tarefa não tenha sido finalizada, o desenvolvedor deve documentar nos comentários da issue, o seu progresso, o que falta ser realizado e os motivos de atraso. Além disso, registra as horas trabalhadas através do comando:

```
# /spend <número><tempo>
/spend 7h
```

### Tarefas Finalizadas

Ao finalizar a tarefa, o desenvolvedor descreve nos comentários da issue o que foi feito, inclui alguma representação do trabalho (print de tela por exemplo) e vincula o número do commit que finaliza a tarefa.

### Transparência

A vantagem dos quadros é possibilitar que qualquer pessoa possa acessá-lo e ficar sabendo de forma rápida, em que cada membro da equipe está trabalhando.
