# Guia de Estilo

Boas práticas na redação do código é como usar uma pontuação correta: você pode passar sem isso, mas com certeza torna a leitura mais fácil. Este guia descreve o estilo a ser utilizado dentro do IPLANFOR/DIOBS, e é baseado no [guia de estilo do Google](google.GitHub.oi/styleguide/Rguide.html).

Um bom estilo é importante porque, embora normalmente o código tenha apenas um autor, ele com frequência tem vários leitores. Isto e especialmente verdade quando você escreve código de forma colaborativa. Assim, de antemão  é importante que haja um acordo  sobre o estilo a ser utilizado.  Como nos existe um estilo melhor do que o outro, ao trabalhar em conjunto, isso significa que você  terá que sacrificar algumas das suas preferências nesse aspecto.

Para ajudar nesta tarefa, você pode usar o pacote formatR.

## Notação e nomenclatura

### Nome de arquivos

- Nomes de arquivos devem ser significativos e terminarem com a extensão .R. 
- Espaços devem ser evitados, pois podem gerar incompatibilidade entre diferentes sistemas operacionais e codificações de texto. Ao invés de espaços,utilize underlines (_)
- Utilize, sempre que possivel, letras minúsculas. Reserve letras maiúsculas para acrônimos
- Se um conjunto de arquivos precisar ser visto/executado em sequencia, coloque um prefixo numérico (reserve dois dígitos para esse caso)
```
00-coleta_dados.R
01-processa_dados.R
02-explora_dados.R
```
### Nome de objetos/variáveis 

“Existem apenas duas coisas difíceis em ciência da computação: invalidar um cache e nomear coisas” (Phil Karlton)”

- Nomes de variáveis e funções devem ser em caixa baixa (minusculas), utilizando underscore para separar palavras em um nome.
- Em geral, nomes de variáveis devem ser substantivos, enquanto nomes de funções devem ser verbos. Tente manter os nomes curtos/concisos mas significativos!
```{r}
# Bom
Dia_1
Dia_um

# Ruim
Primeiro_dia_do_mes
DiaUm
DiaUm
D1
```
- Evite Usar nomes de funções e variáveis existentes. Isso pode causar confusão nos leitores do código e efeitos inesperados na execução do programa!

## Sintaxe
Coloque espaços em torno de todos os operadores infixados (=, +, -, …). A mesma regra se aplica quanto utilizar = ou <- em chamadas de função ou atribuição de variáveis. Sempre coloque um espaço após vírgulas, e nunca antes.

Não coloque espaços ao redor de código dentro de parênteses, a menos que haja uma vírgula.

### Colchetes

Um colchete  que se abre nunca deve estar sozinho em uma linha, e deve sempre estar seguido de um carácteres de nova linha (newline).
Um colchete que se fecha deve sempre estar presente em uma linha própria, a menos que seja seguido por um else.

## Comprimento de linha

Limite o código, sempre que possivel, a 80 carácteres por linha. Isso permite que um eventual código impresso se ajuste de forma confortavel com um tamanho de fonte de fonte legível. Se você estiver achando que o comprimento da linha esta muito curto para o seu código, provavelmente você precisa definir uma nova função =)

## Indentação 

Ao indentar o código, utiliza dois espaços. Nunca utilize TABs ou misture os dois.

A única exceção são os casos em que a definição de uma função precisar ser colocada em múltiplas linhas .

## Atribuição 

Utilize <- ao invés de = para atribuição de variáveis 

## Organização 

Comente o seu código. 